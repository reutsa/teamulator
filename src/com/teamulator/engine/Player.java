package com.teamulator.engine;
import java.util.Random;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Implements a simple player with two parameters. Rank - an integer between 1 and 5.
 * Name - the player's name. This object implements Parcelable so it can be transfered
 * from one activity to the other (parcelable objects can be inserted into Bundles).
 * from one a
 * 
 * @author Ran Romano & Tom Volo.
 *
 */
public class Player implements Comparable<Player>, Parcelable {
	private int rank;					// The rank of the player.
	private String name;				// The name of the player.
	
	/**
	 * Parcelable Creator field to re-construct player objects.
	 */
	public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
		@Override
		public Player createFromParcel(Parcel source) {
			return new Player(source);
		}

		@Override
		public Player[] newArray(int size) {
			return new Player[size];
		}	
	};
	
	/**
	 * Parcel Constructor.
	 * @param pc
	 */
	public Player(Parcel pc) {
		this.rank = pc.readInt();
		this.name = pc.readString();
	}
	
	/**
	 * Default constructor. Null parameters are not handled.
	 * @param rank
	 * @param name
	 */
	public Player(String name, int rank) {
		this.name = name;
		this.rank = rank;
	}
	
	/**
	 * Returns the rank of the player.
	 * @return the rank of the player.
	 */
	public int getRank() {
		return rank;
	}
	
	/**
	 * Sets the rank of the player.
	 * @param rank to set.
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	/**
	 * Returns the name of the player.
	 * @return name of player.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the player.
	 * @param name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Compares two players according to their rank.
	 * If the players have the same rank, randomly chooses who is "better".
	 */
	@Override
	public int compareTo(Player other) {
		if (this.rank == other.rank) {
			Random rand = new Random();
			return rand.nextBoolean() ? 1 : -1;
		} else {
			return this.rank - other.rank;
		}
		
	}
	
	/**
	 * Returns the rank of a player as a string. Used mainly for testing purposes. 
	 */
	public String toString() {
		return this.name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(rank);
		dest.writeString(name);
	}
}
