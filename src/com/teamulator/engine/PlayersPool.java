package com.teamulator.engine;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Represents the pool of players relevant to the game, meaning the players we actually
 * need to divide to teams.
 * @author Ran Romano & Tom Volo.
 *
 */
public class PlayersPool {
	
	
	Player[] players;						// All the relevant players to the game.
	PriorityQueue<Team> workingQueue;		// Two priority queues for the Teamulator algorithm. 	 
	PriorityQueue<Team> loadingQueue; 

	
	/**
	 * Constructor - builds a player pool object from a list of players.
	 * @param playersList
	 */
	public PlayersPool(List<Player> playersList) {
		this.players = new Player[playersList.size()];
		// Change the list to an array, so it will be easier to sort it.
		playersList.toArray(this.players);
		
		// Sorts the players array according to each player's rank.
		Arrays.sort(players);
		
		// Initializes the priority queues.
		workingQueue = new PriorityQueue<Team>();
		loadingQueue = new PriorityQueue<Team>();
	}
	
	/**
	 * The Teamulator algorithm. The main logic is that the weakest team (according to it's 
	 * RankSum) gets the next best player in line. If two teams have the same rank, one is 
	 * randomly selected, in order to get different results after each 'Teamulate' operation.
	 * Note: Works under the assumption that all the players are playing. Example - if there are
	 * 16 players, we can't have 3 teams. 
	 * 
	 * @return Team array. Each team with its designated players.
	 */
	public Team[] teamulate(int playerPerTeam) {
		
		// Initialize the required number of teams.
		int numOfTeams = players.length / playerPerTeam;
		Team[] teams = new Team[numOfTeams];
		
		// Initialize the teams and Add them all to the queue.
		for (int j = 0; j < numOfTeams; j++) {
			teams[j] = new Team(playerPerTeam);
			workingQueue.add(teams[j]);
		}
	
		// Iterating helper boolean for the working and loading queues. 
		boolean iterQueue = true;
		Team temp;
		
		// Going from last to first because we want to appoint the stronger players first.
		for (int i = players.length - 1; i >= 0; i--) {
			temp = iterQueue ? workingQueue.poll() : loadingQueue.poll();
			if (temp != null) {
				
				// Add the player to the next team in the queue, meaning the weakest team.
				try {
					temp.addPlayer(players[i]);
					
					if (iterQueue) {
						loadingQueue.add(temp);
					} else {
						workingQueue.add(temp);
					}
				} catch (IllegalArgumentException e) {
					// Do nothing.
				}
			
			// If one queue has emptied, move to the other, and return a step.
			} else {
				iterQueue = !iterQueue;
				i++;
			}
		}
		return teams;
	}
}
