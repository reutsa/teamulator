package com.teamulator.engine;
import java.util.Random;


/**
 * Represents a Team. Each team has an array of players and a 'RankSum', which is 
 * the sum of the ranks of all the players in the team. the RankSum determines the 
 * strength of the team.
 * 
 * @author Ran Romano & Tom Volo.
 *
 */
public class Team implements Comparable<Team> {
	private int teamSize;					// The desired team size.
	private Player[] players;				// The players in the team.
	private int rankSum;					// The teams RankSum.
	private int currentSize;				// The teams current size.
	
	/**
	 * Builds a empty team with the given size.
	 * @param size
	 */
	public Team(int size) {
		this.teamSize = size;
		players = new Player[teamSize];
		this.rankSum = 0;
		this.currentSize = 0;
	}
	
	/**
	 * Adds a player to the team.
	 * @param player to add.
	 * @throws IllegalArgumentException - if the Team size is full.
	 */
	public void addPlayer(Player player) throws IllegalArgumentException {
		if (currentSize >= teamSize) {
			throw new IllegalArgumentException("The Team is already full!");
		} 
		players[currentSize] = player;
		currentSize++;
		rankSum += player.getRank();
	}
	
	/**
	 * Returns the array of players in the team. Can return an empty array. 
	 * @return The players in the team.
	 */
	public Player[] getPlayers() {
		return this.players;
	}
	
	/**
	 * Compares two teams according to the aggregated rank of their players (RankSum).
	 * Here, the weaker team wins. If the teams have the same rank, randomly chooses
	 * who is "weaker". 
	 */
	@Override
	public int compareTo(Team other) {
		if (this.rankSum == other.rankSum) {
			Random rand = new Random();
			return rand.nextBoolean() ? 1 : -1; 
		} else {
			return this.rankSum - other.rankSum;
		}
	}
	
	/**
	 * Returns the RankSum of the players in the team.
	 * @return rankSum.
	 */
	public int getRankSum() {
		return this.rankSum;
	}
	
	/**
	 * Returns the aggregated rank of the team as a string. Used mainly for testing purposes. 
	 */
	public String toString() {
		return rankSum + "";
	}	
}
