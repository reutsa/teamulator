package com.teamulator.engine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

/**
 * A DB adapter for the players.
 * When creating a new instance of this object, make sure to use the 'open' method
 * before using any other method.
 * 
 * @author Ran Romano.
 *
 */
public class PlayersDBAdapter {
	private final Context context;
	private PlayersDbHelper playersDbHelper;
	private SQLiteDatabase db;

	private static final String DB_NAME = "players";
	private static final int DB_VERSION = 1;
	public static final String DB_TABLE = "playerstable";

	private static final String KEY_ROWID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_RANK = "rank";

	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + DB_TABLE
			+ "(" + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ KEY_NAME + " TEXT NOT NULL, " + KEY_RANK + " INTEGER);";

	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
			+ DB_TABLE;

	/**
	 * The database helper class
	 * 
	 * @author Ran Romano
	 * 
	 */
	private static class PlayersDbHelper extends SQLiteOpenHelper {

		public PlayersDbHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_ENTRIES);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// As for now, only 'reboots' the DB.
			db.execSQL(SQL_DELETE_ENTRIES);
			onCreate(db);
		}
	}

	public PlayersDBAdapter(Context context) {
		this.context = context;
	}

	/**
	 * Use this method right after creating a new instance of the DB.
	 * @return
	 */
	public PlayersDBAdapter open() {
		playersDbHelper = new PlayersDbHelper(context);
		db = playersDbHelper.getWritableDatabase();
		return this;
	}
	
	/**
	 * Closes the DB.
	 */
	public void close() {
		if (playersDbHelper != null) {
			playersDbHelper.close();
		}
	}

	/**
	 * Returns a Cursor object that holds all the players in the pool
	 * 
	 * @return
	 */
	public ArrayList<Player> getAllPlayers() {
		String[] columns = {KEY_ROWID, KEY_NAME, KEY_RANK};
		ArrayList<Player> players = new ArrayList<Player>();

		// Gets a cursor object that holds all the necessary data from the DB.
		Cursor c = db.query(DB_TABLE, 	// players DB to query.
				columns, 				// the 'Name' and 'Rank' keys.
				null, 					// return all of the columns.
				null, 					// return all the values.
				null, 					// don't group the rows.
				null, 					// don't filter by row groups
				null 					// don't sort.
				);

		if (c != null) {
			c.moveToFirst();
			// Re-construct the players from the DB and add them to the array list.
			do {
				players.add(new Player(c.getString(c.getColumnIndex(KEY_NAME)),
						c.getInt(c.getColumnIndex(KEY_RANK))));
				
			} while (c.moveToNext());
			
			// Close the cursor.
			c.close();
		}		
		return players;
	}
	
	/*
	 * Returns a Cursor object that holds the details of the player at the given index.
	 */
	public Cursor getPlayer(int i) {
		String[] columns = {KEY_ROWID, KEY_NAME, KEY_RANK};
		String selection = KEY_ROWID + "= ?";
		String[] selectionArgs = {i + ""}; 
		
		
		Cursor c = db.query(DB_TABLE, columns, selection, selectionArgs, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}

	/**
	 * Adds a new player to the database.
	 * @param name
	 * @param rank
	 */
	public void addNewPlayer(String name, int rank) {
		// Set up a new map of values.
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name);
		values.put(KEY_RANK, rank);

		// Insert the player to the DB.
		db.insert(DB_TABLE, null, values);
	}

	public void reset() {
		playersDbHelper.onUpgrade(db, 1, 1);
		db = playersDbHelper.getWritableDatabase();
	}
	
}
